# Lightweight slider/carousel

The aim of this exercise is to create a basic slider or carousel plugin, the images are in a list in the index.html file.

The slider code should be placed in the file script.js. The test cases are implemented in test.js.

Options can be provided in the instantiation of the carousel 

//example usage
$('.carousel').carousel({options});

To Achieve 

1. Test cases must pass, as provided in test.js. 3 different carousels must be implemented as given in index.html
2. the carousel must take options like 'vertical', 'auto' etc.
