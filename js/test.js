//Do not modify

//Basic carousel with controls
$('.carousel').carousel();

//Self moving carousel with timer
$('.carousel-auto').carousel({
	auto: 'true',
	timer: '3000'
});

//Basic carousel moving Vertically
$('.carousel-vertical').carousel({
	vertical:'true'
});